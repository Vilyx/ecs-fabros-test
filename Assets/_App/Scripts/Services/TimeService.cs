namespace App
{
    public class TimeService 
    {
        public float Time
        {
            get
            {
                return UnityEngine.Time.time;
            }
        }
        public float DeltaTime
        {
            get
            {
                return UnityEngine.Time.deltaTime;
            }
        }
        public float UnscaledDeltaTime
        {
            get
            {
                return UnityEngine.Time.unscaledDeltaTime;
            }
        }
        public float UnscaledTime
        {
            get
            {
                return UnityEngine.Time.unscaledTime;
            }
        }
    }
}