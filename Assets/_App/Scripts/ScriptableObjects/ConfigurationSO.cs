﻿using UnityEngine;

namespace App
{
    [CreateAssetMenu(fileName = "Configuration")]
    public class ConfigurationSO : ScriptableObject
    {
        public float playerSpeed;
        public float cameraFollowSmoothness;
        public float doorOpenDuration;
        public float doorActivationDistance;
    }
}
