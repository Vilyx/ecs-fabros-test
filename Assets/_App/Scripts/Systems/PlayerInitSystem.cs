﻿using Leopotam.EcsLite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class PlayerInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems ecsSystems)
        {
            var ecsWorld = ecsSystems.GetWorld();
            var gameData = ecsSystems.GetShared<GameData>();

            var playerEntity = ecsWorld.NewEntity();

            var playerPool = ecsWorld.GetPool<PlayerComponent>();
            playerPool.Add(playerEntity);
            ref var playerComponent = ref playerPool.Get(playerEntity);
            var playerInputPool = ecsWorld.GetPool<PlayerInputComponent>();
            playerInputPool.Add(playerEntity);
            ref var playerInputComponent = ref playerInputPool.Get(playerEntity);

            var playerAnimationPool = ecsWorld.GetPool<PlayerAnimationComponent>();
            playerAnimationPool.Add(playerEntity);
            ref var playerAnimationComponent = ref playerAnimationPool.Get(playerEntity);

            var playerGO = GameObject.FindGameObjectWithTag(Constants.Tags.Player);
            playerComponent.playerSpeed = gameData.configuration.playerSpeed;
            playerComponent.currentPosition = playerGO.transform.position;

            playerInputComponent.targetPosition = playerComponent.currentPosition;

            playerAnimationComponent.playerAnimator = playerGO.GetComponent<Animator>();
            playerAnimationComponent.playerTransform = playerGO.transform;
        }
    }
}