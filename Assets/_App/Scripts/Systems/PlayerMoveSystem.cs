﻿using App;
using Leopotam.EcsLite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class PlayerMoveSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems ecsSystems)
        {
            var filter = ecsSystems.GetWorld().Filter<PlayerComponent>().Inc<PlayerInputComponent>().End();
            var playerPool = ecsSystems.GetWorld().GetPool<PlayerComponent>();
            var playerInputPool = ecsSystems.GetWorld().GetPool<PlayerInputComponent>();

            foreach (var entity in filter)
            {
                ref var playerComponent = ref playerPool.Get(entity);
                ref var playerInputComponent = ref playerInputPool.Get(entity);

                var currentPosition = playerComponent.currentPosition;
                var targetPosition = playerInputComponent.targetPosition;
                var movementVector = targetPosition - currentPosition;
                var movementDirection = movementVector.normalized;

                float distanceToTarget = movementVector.magnitude;
                var motion = movementDirection * Mathf.Min(playerComponent.playerSpeed, distanceToTarget);
                var newPosition = currentPosition + motion;
                playerComponent.currentPosition = newPosition;
            }
            
        }
    }
}