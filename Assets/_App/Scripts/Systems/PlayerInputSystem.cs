﻿using Leopotam.EcsLite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class PlayerInputSystem : IEcsRunSystem
    {
        private Plane _plane = new Plane(Vector3.up,Vector3.zero);

        public void Run(IEcsSystems ecsSystems)
        {
            var filter = ecsSystems.GetWorld().Filter<PlayerInputComponent>().End();
            var playerInputPool = ecsSystems.GetWorld().GetPool<PlayerInputComponent>();
            //var gameData = ecsSystems.GetShared<GameData>();

            foreach (var entity in filter)
            {
                ref var playerInputComponent = ref playerInputPool.Get(entity);


                if (Input.GetMouseButtonDown(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    if (_plane.Raycast(ray, out var enter))
                    {
                        Vector3 hitPoint = ray.GetPoint(enter);
                        playerInputComponent.targetPosition = hitPoint;
                    }
                }
            }
        }
    }
}