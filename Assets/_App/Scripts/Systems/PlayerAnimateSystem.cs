﻿using Leopotam.EcsLite;
using UnityEngine;

namespace App
{
    public class PlayerAnimateSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems ecsSystems)
        {
            var filter = ecsSystems.GetWorld().Filter<PlayerComponent>().Inc<PlayerInputComponent>().Inc<PlayerAnimationComponent>().End();
            var playerPool = ecsSystems.GetWorld().GetPool<PlayerComponent>();
            var playerInputPool = ecsSystems.GetWorld().GetPool<PlayerInputComponent>();
            var playerAnimationPool = ecsSystems.GetWorld().GetPool<PlayerAnimationComponent>();

            foreach (var entity in filter)
            {
                ref var playerComponent = ref playerPool.Get(entity);
                ref var playerInputComponent = ref playerInputPool.Get(entity);
                ref var playerAnimationComponent = ref playerAnimationPool.Get(entity);

                var currentPosition = playerComponent.currentPosition;
                var targetPosition = playerInputComponent.targetPosition;
                var movementVector = targetPosition - currentPosition;

                float distanceToTarget = movementVector.magnitude;
                playerAnimationComponent.playerAnimator.SetFloat("MoveSpeed", Mathf.Min(distanceToTarget, playerComponent.playerSpeed)*100);
                playerAnimationComponent.playerTransform.position = currentPosition;
                if (distanceToTarget > float.Epsilon)
                {
                    playerAnimationComponent.playerTransform.rotation = Quaternion.LookRotation(movementVector.normalized, Vector3.up);
                }
            }
        }
    }
}