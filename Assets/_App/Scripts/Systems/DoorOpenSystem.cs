﻿using Leopotam.EcsLite;
using LeopotamGroup.Globals;
using UnityEngine;
using UnityEngine.LowLevel;

namespace App
{
    public class DoorOpenSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems ecsSystems)
        {
            var gameData = ecsSystems.GetShared<GameData>();

            var filterDoors = ecsSystems.GetWorld().Filter<DoorComponent>().End();
            var filterPlayers = ecsSystems.GetWorld().Filter<PlayerComponent>().End();
            var doorPool = ecsSystems.GetWorld().GetPool<DoorComponent>();
            var playerPool = ecsSystems.GetWorld().GetPool<PlayerComponent>();
            float doorActivationDistance = gameData.configuration.doorActivationDistance;
            float doorOpenSpeed = 1 / gameData.configuration.doorOpenDuration * Service<TimeService>.Get().DeltaTime;
            foreach (var doorEntity in filterDoors)
            {
                ref var doorComponent = ref doorPool.Get(doorEntity);
                Vector3 buttonPosition = doorComponent.buttonPosition; 
                foreach (var playerEntity in filterPlayers)
                {
                    ref var playerComponent = ref playerPool.Get(playerEntity);
                    Vector3 playerPosition = playerComponent.currentPosition;
                    float distancePlayerToDoorButton = Vector3.Distance(buttonPosition, playerPosition);

                    if (distancePlayerToDoorButton < doorActivationDistance)
                    {
                        doorComponent.doorOpenProgress += doorOpenSpeed;
                    }
                }
            }

        }
    }
}