﻿using Leopotam.EcsLite;
using UnityEngine;

namespace App
{
    public class DoorInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems ecsSystems)
        {
            var ecsWorld = ecsSystems.GetWorld();
            var gameData = ecsSystems.GetShared<GameData>();

            foreach (var doorGO in GameObject.FindGameObjectsWithTag(Constants.Tags.Door))
            {
                var doorEntity = ecsWorld.NewEntity();

                var doorPool = ecsWorld.GetPool<DoorComponent>();
                doorPool.Add(doorEntity);
                ref var doorComponent = ref doorPool.Get(doorEntity);
                var doorAnimationPool = ecsWorld.GetPool<DoorAnimationComponent>();
                doorAnimationPool.Add(doorEntity);
                ref var doorAnimationComponent = ref doorAnimationPool.Get(doorEntity);

                var doorTransform = doorGO.transform;
                doorAnimationComponent.doorClosedPosition = doorTransform.position;
                doorAnimationComponent.doorOpenPosition = doorTransform.position + Vector3.up * 1;
                doorAnimationComponent.doorTransform = doorTransform;
                foreach (Transform child in doorTransform)
                {
                    if (child.tag == Constants.Tags.Button)
                    {
                        doorComponent.buttonPosition = child.position;
                        child.parent = null;
                        break;
                    }
                }
            }
        }
    }
}