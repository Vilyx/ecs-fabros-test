﻿using Leopotam.EcsLite;
using UnityEngine;

namespace App
{
    public class DoorAnimationSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems ecsSystems)
        {
            var filterDoors = ecsSystems.GetWorld().Filter<DoorComponent>().Inc<DoorAnimationComponent>().End();
            var doorPool = ecsSystems.GetWorld().GetPool<DoorComponent>();
            var doorAnimationPool = ecsSystems.GetWorld().GetPool<DoorAnimationComponent>();

            foreach (var doorEntity in filterDoors)
            {
                ref var doorComponent = ref doorPool.Get(doorEntity);
                ref var doorAnimationComponent = ref doorAnimationPool.Get(doorEntity);

                Vector3 closedPos = doorAnimationComponent.doorClosedPosition;
                Vector3 openPos = doorAnimationComponent.doorOpenPosition;
                doorAnimationComponent.doorTransform.position = Vector3.Lerp(closedPos, openPos, doorComponent.doorOpenProgress);
            }
        }
    }
}