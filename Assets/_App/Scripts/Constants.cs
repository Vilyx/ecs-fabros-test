﻿public static class Constants
{
    public static class Tags
    {
        public const string Door = "Door";
        public const string Button = "Button";
        public const string Player = "Player";
    }
}