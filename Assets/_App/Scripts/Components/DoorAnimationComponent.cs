﻿using UnityEngine;

public struct DoorAnimationComponent
{
    public Transform doorTransform;
    public Vector3 doorClosedPosition;
    public Vector3 doorOpenPosition;
}