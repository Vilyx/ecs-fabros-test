﻿using UnityEngine;

public struct DoorComponent
{
    public Vector3 buttonPosition;
    public float doorOpenProgress;
}
