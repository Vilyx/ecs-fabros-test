﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayerComponent
{
    public Vector3 currentPosition;
    public float playerSpeed;
}
