﻿using UnityEngine;

public struct PlayerAnimationComponent
{
    public Transform playerTransform;
    public Animator playerAnimator;
}