using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class GameData
    {
        public ConfigurationSO configuration;
        public GameObject playerWonPanel;
        public GameObject gameOverPanel;
        public TimeService timeService;
    }
}
