using Leopotam.EcsLite;
using LeopotamGroup.Globals;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace App
{
    public class Startup : MonoBehaviour
    {
        private EcsWorld ecsWorld;
        private IEcsSystems initSystems;
        private IEcsSystems updateSystems;
        private IEcsSystems fixedUpdateSystems;
        [SerializeField] private ConfigurationSO configuration;

        private void Start()
        {
            ecsWorld = new EcsWorld();
            var gameData = new GameData();

            gameData.configuration = configuration;
            gameData.timeService = Service<TimeService>.Get(true);

            initSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerInitSystem())
                .Add(new DoorInitSystem());

            initSystems.Init();

            updateSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerInputSystem())
                .Add(new PlayerAnimateSystem())
                .Add(new DoorOpenSystem())
                .Add(new DoorAnimationSystem());

            updateSystems.Init();

            fixedUpdateSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerMoveSystem())
                .Add(new CameraFollowSystem());

            fixedUpdateSystems.Init();
        }

        private void Update()
        {
            updateSystems.Run();
        }

        private void FixedUpdate()
        {
            fixedUpdateSystems.Run();
        }

        private void OnDestroy()
        {
            initSystems.Destroy();
            updateSystems.Destroy();
            fixedUpdateSystems.Destroy();
            ecsWorld.Destroy();
        }
    }
}
